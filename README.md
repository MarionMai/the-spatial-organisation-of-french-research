# The spatial organisation of French research 

R scripts used for the conference article "Spatial organisation of French research from the scholarly publication standpoint (1999-2017): Long-standing dynamics and policy-induced disorder"

Conference speach done in Paris by Michel Grosseti at the "Complexité et Désordre" conference of Physics, January 2020, click [here](http://geoscimo.univ-tlse2.fr/presentation-sur-le-cas-francais-au-colloque-complexite-et-desordre/) to see the slides. 
Data, analyses and text of the conference article revised in April-May 2020 for a publication in the Web of Conference